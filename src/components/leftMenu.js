import React, {Component} from 'react';
import {TouchableOpacity, Text} from 'react-native';
import Menu from 'react-native-material-menu';

export default class CustomMenu extends Component {
  _menu = null;

  setMenuRef = (ref) => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  render() {
    return (
      <Menu
        ref={(ref) => (this._menu = ref)}
        button={
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('BiltiSearch')}
            style={{
              paddingHorizontal: 16,
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: 'white'}}>Scan</Text>
          </TouchableOpacity>
        }></Menu>
    );
  }
}
