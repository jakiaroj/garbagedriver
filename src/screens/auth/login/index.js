import React, {Component} from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {TextInput} from 'react-native-paper';
import Button from '../../../components/defaultButton';
import colors from '../../../config/colors';
import styles from './styles';
import axios from '../../../axios';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      loading: false,
    };
  }

  handleLogin = async () => {
    console.log('here');
    if (this.state.email === '' || this.state.password === '') {
      alert('Email or Password is empty!!');
    } else {
      await this.setState({
        loading: true,
      });
      try {
        let login = await axios.post(`api/login`, {
          email: this.state.email,
          password: this.state.password,
        });
        console.log('logindata', login.data.success);
        let success = login.data.success;
        //let user = await axios.get(`api/consignees/${login.data.userId}`);

        await AsyncStorage.setItem('name', success.user_data.name);
        await AsyncStorage.setItem('email', success.user_data.email);
        await AsyncStorage.setItem('userId', success.user_data.id.toString());
        await AsyncStorage.setItem('user_status', success.user_data.user_status.toString());
        await AsyncStorage.setItem('accessToken', success.token);
        await this.setState({
          loading: false,
        });
        this.props.navigation.navigate('App');
      } catch (e) {
        console.log('e', e.response);
        await this.setState({
          loading: false,
        });
        alert('Invalid Login');
      }
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image
            source={require('../../../assets/logo.png')}
            style={{width: 150, height: 150, marginTop: 30, marginBottom: 10}}
            resizeMode="contain"
          />
          <Text
            style={{
              marginTop: 10,
              marginBottom: 10,
              fontSize: 20,
              fontWeight: 'bold',
            }}>
            Garbage Driver
          </Text>
        </View>

        <TextInput
          mode="outlined"
          label="Email"
          keyboardType="email-address"
          autoCapitalize="none"
          returnKeyType="next"
          value={this.state.email}
          onChangeText={text => this.setState({email: text})}
          style={{marginBottom: 10}}
        />

        <TextInput
          mode="outlined"
          label="Password"
          autoCapitalize="none"
          value={this.state.password}
          onChangeText={text => this.setState({password: text})}
          secureTextEntry
          style={{marginBottom: 10}}
        />

        <Button
          color={colors.success}
          onPress={this.handleLogin}
          loading={this.state.loading}
          disabled={this.state.loading}>
          LOGIN
        </Button>
      </View>
    );
  }
}

export default Login;
