import {StyleSheet} from 'react-native';
import colors from "../../../config/colors";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  forgotPasswordContainer: {paddingBottom: 16, justifyContent: 'center'},
  forgotPassword: {textDecorationLine: "underline", color: colors.primary}
});

export default styles;
