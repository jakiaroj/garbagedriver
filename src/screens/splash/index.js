import React, { PureComponent } from 'react';
import { View, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
class Splash extends PureComponent {
  async componentDidMount() {
    //await AsyncStorage.removeItem('marka');
    const userToken = await AsyncStorage.getItem('accessToken')
    if (userToken === null) {
      this.props.navigation.navigate('Auth')
    } else {
      this.props.navigation.navigate('App')
    }
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator animating={true} size="large" />
      </View>
    );
  }
}

export default Splash;
