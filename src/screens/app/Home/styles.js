import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchBox: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16,
    marginBottom: 20,
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontWeight: 'bold',
  },
  card: {
    backgroundColor: '#2682B1',
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    marginBottom: 10,
  },
  button: {
    marginBottom: 10,
  },
});

export default styles;
