import React, {PureComponent} from 'react';
import {View, Text, ActivityIndicator} from 'react-native';
import {RNCamera} from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import colors from '../../../config/colors';
import AsyncStorage from '@react-native-community/async-storage';
import axios from '../../../axios';

class ScanPay extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      scanned: false,
    };
  }

  async componentDidMount() {
    const userId = await AsyncStorage.getItem('userId');

    this.setState({
      userId: userId,
      accessToken: accessToken,
      focusedScreen: true,
    });
  }
  componentDidMount() {
    const {navigation} = this.props;
    navigation.addListener('willFocus', () =>
      this.setState({focusedScreen: true}),
    );
    navigation.addListener('willBlur', () =>
      this.setState({focusedScreen: false}),
    );
  }

  onRead = async (e) => {
    await this.setState({
      scanned: true,
    });
    console.log('Read QR: ', e.data);
    let manager_id = e.data;
    let data = {
      manager_id: manager_id,
    };

    try {
      const accessToken = await AsyncStorage.getItem('accessToken');
      console.log('acce', accessToken);
      await axios.post('api/complete_alert', data, {
        headers: {
          Authorization: 'Bearer ' + accessToken,
        },
      });
      this.setState({
        scanned: false,
      });
      alert('Collection Completed');
    } catch (e) {
      alert('bhayena');
      console.log('err', e.response);
    }
  };

  static navigationOptions = {
    title: 'Scan to Complete',
    headerStyle: {
      backgroundColor: colors.primary,
      borderWidth: 0,
    },
    headerTitleStyle: {
      fontWeight: '200',
    },
    headerTintColor: '#fff',
  };
  render() {
    return (
      <View style={{flex: 1}}>
        {!this.state.scanned ? (
          <RNCamera
            ref={(ref) => {
              this.camera = ref;
            }}
            style={{
              flex: 1,
            }}
            type={RNCamera.Constants.Type.back}
            autoFocus="on"
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            onBarCodeRead={this.onRead}
            captureAudio={false}>
            <BarcodeMask edgeColor={colors.primary} showAnimatedLine={false} />
          </RNCamera>
        ) : (
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator size="small" color="black" />
            <Text>Scanning</Text>
          </View>
        )}
      </View>
    );
  }
}

export default ScanPay;
