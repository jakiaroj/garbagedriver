import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  FlatList,
  ScrollView,
} from 'react-native';
import styles from './styles';
import {Card, CardItem, Left, Button, Right, Icon} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import axios from '../../../axios';
import moment from 'moment';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      user_status: '',
      loading: true,
      history: [],
    };
  }
  async componentDidMount() {
    const name = await AsyncStorage.getItem('name');
    const user_status = await AsyncStorage.getItem('user_status');
    this.setState({
      name: name,
      user_status: user_status,
    });
    this.getHistory();
  }

  getHistory = async () => {
    await this.setState({
      loading: true,
    });
    try {
      const accessToken = await AsyncStorage.getItem('accessToken');
      let history = await axios.get('api/driver_active_jobs', {
        headers: {
          Authorization: 'Bearer ' + accessToken,
        },
      });
      this.setState({
        history: history.data.success,
      });
      console.log('history', history.data);
    } catch (e) {
      alert('Couldnt get history');
    }
    await this.setState({
      loading: false,
    });
  };

  alertPressed = (color) => {
    Alert.alert(
      'Are you Sure?',
      `Send ${color.toUpperCase()} alert ?`,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: async () => {
            this.setState({
              loading: true,
            });
            try {
              const userId = await AsyncStorage.getItem('userId');
              const locationId = await AsyncStorage.getItem('locationId');
              const accessToken = await AsyncStorage.getItem('accessToken');
              let post = await axios.post(
                `api/create_collection_request`,
                {
                  manager_id: userId,
                  type: color,
                  message: 'This is a test',
                  location_id: locationId,
                },
                {
                  headers: {
                    Authorization: 'Bearer ' + accessToken,
                  },
                },
              );
              alert('Alert Successfully Sent');
              console.log('post', post.data);
            } catch (e) {
              alert('Ooops. Failed');
            }
            this.setState({
              loading: false,
            });
          },
        },
      ],
      {cancelable: true},
    );
  };
  render() {
    return (
      <View style={styles.container}>
        {this.state.loading ? (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator animating={true} size="large" />
            <Text>Fetching Data...</Text>
          </View>
        ) : (
          <ScrollView>
            <View>
              <Card style={styles.card}>
                <CardItem style={styles.card}>
                  <Left>
                    <Text style={styles.text}>Welcome {this.state.name}</Text>
                  </Left>
                  <Right>
                    <TouchableOpacity
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                      onPress={() =>
                        this.props.navigation.navigate('BiltiList', {
                          type: 'History',
                        })
                      }>
                      <Text style={styles.text}>History</Text>
                    </TouchableOpacity>
                  </Right>
                </CardItem>
              </Card>
              <View style={styles.logoContainer}>
                <Image
                  source={require('../../../assets/logo.png')}
                  style={{
                    width: 100,
                    height: 100,
                    marginTop: 20,
                    marginBottom: 10,
                  }}
                  resizeMode="contain"
                />
                <Text
                  style={{
                    marginTop: 10,
                    marginBottom: 0,
                    fontSize: 20,
                    fontWeight: 'bold',
                  }}>
                  Garbage Driver
                </Text>
                <Text
                  style={{
                    marginTop: 10,
                    marginBottom: 10,
                    fontSize: 15,
                    fontWeight: 'bold',
                  }}>
                  Status :{' '}
                  {this.state.user_status === '0' ? 'Inactive' : 'Active'}
                </Text>
                <TouchableOpacity onPress={this.getHistory}>
                  <Text
                    style={{
                      marginTop: 10,
                      marginBottom: 10,
                      fontSize: 15,
                      fontWeight: 'bold',
                    }}>
                    Refresh
                  </Text>
                </TouchableOpacity>
              </View>

              <FlatList
                data={this.state.history}
                renderItem={({item, index}) => (
                  <Card>
                    <CardItem header bordered>
                      <Left>
                        <Text style={styles.headerText}>
                          {index + 1}.{' '}
                          {moment(item.created_at).format(
                            'MMMM Do YYYY, h:mm:ss a',
                          )}
                        </Text>
                      </Left>
                    </CardItem>
                    <CardItem bordered>
                      <Left>
                        <Text>Alert Status</Text>
                      </Left>
                      <Right>
                        <Text style={{fontWeight: 'bold'}}>
                          {item.status === 0
                            ? 'Approval Pending'
                            : item.status === 1
                            ? 'Driver Notified'
                            : 'Completed'}
                        </Text>
                      </Right>
                    </CardItem>
                    <CardItem bordered>
                      <Left>
                        <Text>Driver Assigned</Text>
                      </Left>
                      <Right>
                        <Text>
                          {item.driver_name ? item.driver_name : 'Not Assigned'}
                        </Text>
                      </Right>
                    </CardItem>
                    <CardItem bordered>
                      <Left>
                        <Text>Location</Text>
                      </Left>
                      <Right>
                        <Text>{item.location_name}</Text>
                      </Right>
                    </CardItem>
                    <CardItem bordered>
                      <Left>
                        <Text>Alerted By</Text>
                      </Left>
                      <Right>
                        <Text>{item.manager_name}</Text>
                      </Right>
                    </CardItem>
                    <CardItem bordered>
                      <Left>
                        <Text>Message</Text>
                      </Left>
                      <Right>
                        <Text>{item.message}</Text>
                      </Right>
                    </CardItem>
                    <CardItem bordered>
                      <Left>
                        <Text>Alert Type</Text>
                      </Left>
                      <Right>
                        <Text style={{color: item.type, fontWeight: 'bold'}}>
                          {item.type.toUpperCase()}
                        </Text>
                      </Right>
                    </CardItem>
                  </Card>
                )}
              />
            </View>
          </ScrollView>
        )}
      </View>
    );
  }
}

export default Home;
