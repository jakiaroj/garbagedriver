import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 5
    },
    buttonStyle: {
        backgroundColor: "#279745",
         padding: 10
    },
    insideButtonText: {
        color: "#fff"
    },
    headerText: {
        fontSize: 20,
        fontWeight: "bold"
    }
});

export default styles;