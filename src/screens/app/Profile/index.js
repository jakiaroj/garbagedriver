import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Button from '../../../components/defaultButton';
import colors from '../../../config/colors';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      location: '',
      email: '',
      loading: false,
    };
  }
  async componentDidMount() {
    let name = await AsyncStorage.getItem('name');
    let email = await AsyncStorage.getItem('email');

    this.setState({
      name: name,
      email: email,
      location: location,
    });
  }

  logout = async () => {
    await this.setState({
      loading: true,
    });
    await AsyncStorage.removeItem('accessToken');
    await AsyncStorage.removeItem('name');
    await AsyncStorage.removeItem('email');
    await AsyncStorage.removeItem('user_status');
    await AsyncStorage.removeItem('userId');

    await this.setState({
      loading: false,
    });
    this.props.navigation.navigate('Auth');
  };

  static navigationOptions = {
    title: 'Profile Details',
    headerStyle: {
      backgroundColor: colors.primary,
      borderWidth: 0,
    },
    headerTitleStyle: {
      fontWeight: '200',
    },
    headerTintColor: '#fff',
  };
  render() {
    const {name, location, email} = this.state;
    return (
      <View style={styles.container}>
       <Text>Name : {this.state.name}</Text>
       <Text>Email : {this.state.email}</Text>
        <View style={{height: 16}} />

        <Button
          warning
          onPress={this.logout}
          loading={this.state.loading}
          disabled={this.state.loading}>
          Logout
        </Button>
      </View>
    );
  }
}

export default Profile;
