import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {RNCamera} from 'react-native-camera';

export default class App extends Component {
  render() {
    return (
      <View>
       <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={{
              flex: 1,
            }}
            type={RNCamera.Constants.Type.back}
            autoFocus="on"
            androidCameraPermissionOptions={{
              title: "Permission to use camera",
              message: "We need your permission to use your camera",
              buttonPositive: "Ok",
              buttonNegative: "Cancel"
            }}
            //onBarCodeRead={this.onRead}
            captureAudio={false}
          >
          </RNCamera>
      </View>
    );
  }
}
