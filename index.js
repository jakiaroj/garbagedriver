import React from "react";
import { AppRegistry } from 'react-native';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import App from './src/nav';
import { name as appName } from './app.json';
import colors from "./src/config/colors";

console.disableYellowBox = true;



const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.primary,
    accent: '#fff',
  },
};


Main = () => {
  return (
      <PaperProvider theme={theme}>
        <App />
      </PaperProvider>
  );
}

AppRegistry.registerComponent(appName, () => Main);
